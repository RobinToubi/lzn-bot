package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"regexp"

	"github.com/bwmarrin/discordgo"
	"github.com/joho/godotenv"
)

type TwitterMessage struct {
	TweetId string
	Account string
}

const (
	TWITTER_REGEX string = `(?m)https?://(twitter|x)\.com/(?:\#!/)?(\w+)/status(es)?/(\d+)`
)

func main() {
	if err := godotenv.Load(); err != nil {
		fmt.Println("Error when loading env")
	}
	session, err := discordgo.New("Bot " + os.Getenv("TOKEN"))
	if err != nil {
		panic(err)
	}

	defer session.Close()

	session.AddHandler(messageCreate)

	err = session.Open()
	if err != nil {
		log.Fatalf("Cannot open the session: %v", err)
	}

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	log.Println("Press Ctrl+C to exit")
	<-stop
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	regex := regexp.MustCompile(TWITTER_REGEX)
	if m.Author.Bot || !regex.MatchString(m.Content) {
		return
	}
	err := s.ChannelMessageDelete(m.ChannelID, m.Message.ID)
	if err != nil {
		fmt.Println(err)
	}
	newUrl := transformedMessage(m.Message.Content)
	s.ChannelMessageSend(m.ChannelID, newUrl)
}

func transformedMessage(message string) string {
	var re = regexp.MustCompile(TWITTER_REGEX)
	var substitution = "https://fx$1.com/$2/status/$4"

	value := re.ReplaceAllString(message, substitution)
	return value
}
